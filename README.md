# public-molecule-ressources-testing

Public repository used to test ansible roles requiring external data or files with molecule.

## Getting started


## License
Most of theses files are made from random data (/udev/random) for privacy/security reasons.
Theses files ans data are under GPL licenses.

Other files may come from open source projects like docker, openssl, so you need to refer to those licenses.
